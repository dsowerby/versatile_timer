# Examples

In the examples below, various instances of `ExampleMonitor` are used to track the output, see the '
examples' folder.

## Fixed Input

A `FixedTimer` encapsulates the configuration of a `VersatileTimer` with a `FixedFilter`. This uses
a fixed value input, which is then transferred to the output via an optional transformer.

### Example 1

This example outputs a stream of 3 'Ho' Strings at a 10ms intervals.

```dart
Future<void> runLimitedFixedInputTimer() async {
  final timer = FixedTimer<String, String>(
    tickInterval: const Duration(milliseconds: 10),
    inputValue: 'Ho',
    maxTicks: 3,
  );
  final monitor = ExampleMonitor<String, String>(timer);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilFinished();
}
```

### Example 2

Here we use a transformer (Inhibit1) to do two things - to modify the output, and also mute the
output
by returning null when the count is 1

```dart
class Inhibit1 implements FixedTransform<String, String> {
  @override
  String? transform({required int count, required String input}) {
    final String? r = count == 1 ? null : '$input:$count';
    return r;
  }
}

class OddNumbersOnly implements FixedTransform<int, int> {
  @override
  int? transform({required int count, required int input}) {
    final int r = count % 2;
    return r == 0 ? null : count;
  }
}

Future<void> runLimitedFixedInputTimerTransformed() async {
  final timer = FixedTimer<String, String>(
    tickInterval: const Duration(milliseconds: 10),
    inputValue: 'Ho',
    maxTicks: 3,
    transformer: Inhibit1(),
  );
  final monitor = ExampleMonitor<String, String>(timer);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilFinished();
}

```

## Data Input

This implementation takes a list of data as input and then uses the timer to read through the items
and output the item via an optional transformer. The timer's overall count is used as an index to
lookup data items. Without a transformer, as shown here, the data is passed directly to the output.

### Example

This example will generate the toString representation of the three Data items, each 100ms apart on
the output stream.

```dart
Future<void> runIndexedTimer() async {
  final timer = IndexedTimer<Data, Data>(
    data: const [
      Data(quantity: 1, product: 'a'),
      Data(quantity: 2, product: 'b'),
      Data(quantity: 3, product: 'c'),
    ],
    tickInterval: const Duration(milliseconds: 100),
  );
  final monitor = ExampleMonitor<Data, Data>(timer);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilFinished();
}
```

## Pattern Input

This approach uses a `PatternConfig` to define a potentially deeply nested structure as a pattern
for output. This could be used to generate an output of considerable complexity, with sections
of it being repeated as required.  `WeightedTimer` provides a good example of this, and it outputs
a stream of values of variable 'weight'.

This, or a variation of this, could be used to control for example, volume or intensity.

```dart
Future<void> runWeightedTimerWithGroups() async {
  final timer = WeightedTimer<Weight>(
    tickInterval: const Duration(milliseconds: 100),
    config: const WeightedConfig(
      repeat: 2,
      pattern: [
        Group(
          label: 'Group A',
          pattern: [
            Group(label: 'Group B',
              pattern: [
                Weight(10, repeat: 3, label: 'step B1'),
                Weight(20, repeat: 2, label: 'step B2'),
              ],
            ),
            Weight(1, repeat: 2, label: 'step A1'),
            Weight(3, repeat: 4, label: 'step A2'),
          ],
        ),
      ],
    ),
    transformer: const WeightTransformer(),
  );
  final monitor = ExampleMonitor<Weight, Weight>(timer);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilFinished();
}
```