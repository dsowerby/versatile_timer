// ignore_for_file: avoid_print

import 'package:versatile_timer/versatile_timer.dart';

/// [WeightedTimer] is an example of [VersatileTimer] using a pattern.  The pattern
/// itself is defined by [WeightedConfig], an implementation of [PatternConfig].
///
/// In this case we are defining a 'flat' pattern of weights, with no depth
/// to the configuration structure.
///
/// Here we use a [WeightValueTransformer] to transform a tick into just the weight
/// property of the [Weight] configuration object.
///
/// With repeat values as they are the output will be:
/// 1,1,3,3,3,3,1,1,3,3,3,3
///
/// Note that the config has its own repeat value.
///
/// See [runWeightedTimerWithGroups] for a more complex example
Future<void> runWeightedTimer() async {
  print('runWeightedTimer');
  final timer = WeightedTimer<int>(
    tickInterval: const Duration(milliseconds: 100),
    config: const WeightedConfig(
      repeat: 2,
      pattern: [
        Weight(1, repeat: 2, label: 'step 1'),
        Weight(3, repeat: 4),
      ],
    ),
    transformer: const WeightValueTransformer(),
  );
  final monitor = ExampleMonitor<Weight,int>(timer);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilFinished();

  print('total ticks: ${monitor.count}');
  print('----');
}

/// Now we add a [Group] within a [Group] to provide a structure to the configuration.
/// With this, we can define almost any pattern with repeating elements at any level.
///
/// We have also changed the transformer to a [WeightTransformer]. This will output
/// the [Weight] config object itself.
///
/// You can see the output by running the main() method
Future<void> runWeightedTimerWithGroups() async {
  print('runWeightedTimerWithGroups');
  final timer = WeightedTimer<Weight>(
    tickInterval: const Duration(milliseconds: 100),
    config: const WeightedConfig(
      repeat: 2,
      pattern: [
        Group(
          label: 'Group A',
          pattern: [
            Group(label: 'Group B',
              pattern: [
                Weight(10, repeat: 3,label: 'step B1'),
                Weight(20, repeat: 2,label: 'step B2'),
              ],
            ),
            Weight(1, repeat: 2, label: 'step A1'),
            Weight(3, repeat: 4, label: 'step A2'),
          ],
        ),
      ],
    ),
    transformer: const WeightTransformer(),
  );
  final monitor = ExampleMonitor<Weight,Weight>(timer);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilFinished();

  print('total ticks: ${monitor.count}');
  print('----');
}

void main() async {
  await runWeightedTimer();
  await runWeightedTimerWithGroups();
}

class ExampleMonitor<IN,OUT> {
  ExampleMonitor(this.timer, {this.stopAfter = -1});

  final int stopAfter;

  final VersatileTimer<IN,OUT> timer;
  int count = 0;

  void monitor(Stream<OUT> stream) {
    stream.listen((data) {
      count++;
      // ignore: noop_primitive_operations
      print(data.toString());
      if (stopAfter != -1 && count >= 5) {
        timer.stop();
      }
    });
  }
}
