// ignore_for_file: avoid_print

import 'dart:async';

import 'package:versatile_timer/versatile_timer.dart';

/// You can run these examples (using main()) to see the output
///
/// This implementation takes each data element in turn and outputs it as it is.
/// We could add a transformer, in which case the output could be anything you
/// specify in the transformer.
///
/// This example will generate the toString representation of the three Data items,
/// of the "a,b,c" each 100ms apart on the output stream.
///
/// maxTicks can be used to limit the number of ticks processed, or can be left
/// unspecified to run through the entire list, and then stop automatically.
///
/// You can call stop, pause and resume after start has been called, or
/// use timer.waitForFinish() as this example does
///
Future<void> runIndexedTimer() async {
  print('runIndexedTimer');
  final timer = IndexedTimer<Data, Data>(
    data: const [
      Data(quantity: 1, product: 'a'),
      Data(quantity: 2, product: 'b'),
      Data(quantity: 3, product: 'c'),
    ],
    tickInterval: const Duration(milliseconds: 100),
  );
  final monitor = ExampleMonitor<Data,Data>(timer);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilFinished();

  print('total ticks: ${monitor.count}');
  print('----');
}

class ExampleMonitor<IN,OUT> {
  ExampleMonitor(this.timer, {this.stopAfter = -1});

  final int stopAfter;

  final VersatileTimer<IN,OUT> timer;
  int count = 0;

  void monitor(Stream<OUT> stream) {
    stream.listen((data) {
      count++;
      // ignore: noop_primitive_operations
      print(data.toString());
      if (stopAfter != -1 && count >= 5) {
        timer.stop();
      }
    });
  }
}

class Data {
  const Data({
    required this.quantity,
    required this.product,
  });

  final int quantity;
  final String product;

  @override
  String toString() {
    return '{product: $product, quantity: $quantity}';
  }
}

void main() async {
  await runIndexedTimer();
}
