// ignore_for_file: avoid_print

import 'dart:async';

import 'package:versatile_timer/versatile_timer.dart';

/// You can run these examples (using main()) to see the output
///
/// In this example, the timer generates the [inputValue] 'Ho' 3 times, at an
/// interval of 10ms. The interval is set by [tickInterval] and the limit set
/// set by [maxTicks].

/// No [transformer] has been set, so the [inputValue] is passed unchanged to
/// the output.
///
/// You can call stop, pause and resume after start has been called, or
/// use timer.waitForFinish() as this example does.  Note that the
/// timer.waitForDone() method would complete regardless of whether the timer
/// finishes or is stopped.
///
Future<void> runLimitedFixedInputTimer() async {
  print('runLimitedFixedInputTimer');

  final timer = FixedTimer<String,String>(
    tickInterval: const Duration(milliseconds: 10),
    inputValue: 'Ho',
    maxTicks: 3,
  );
  final monitor = ExampleMonitor<String,String>(timer);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilFinished();

  print('total ticks: ${monitor.count}');
  print('----');
}

/// This time we will modify the output with a [transformer].  In this case,
/// the [transformer] appends the tick count to the output.  The [transformer]
/// also returns null when the tick count is 1, and this causes the
/// output to be muted for that tick.

class Inhibit1 implements FixedTransform<String,String> {
  @override
  String? transform({required int count, required String input}) {
    final String? r= count == 1 ? null : '$input:$count';
    return r;
  }
}
class OddNumbersOnly implements FixedTransform<int, int> {
  @override
  int? transform({required int count, required int input}) {
    final int r = count % 2;
    return r == 0 ? null : count;
  }
}
Future<void> runLimitedFixedInputTimerTransformed() async {
  print('runLimitedFixedInputTimerTransformed');


  final timer = FixedTimer<String,String>(
    tickInterval: const Duration(milliseconds: 10),
    inputValue: 'Ho',
    maxTicks: 3,
    transformer: Inhibit1(),
  );
  final monitor = ExampleMonitor<String,String>(timer);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilFinished();

  print('total ticks: ${monitor.count}');
  print('----');
}


/// In this example we make the timer run continuously, by setting continuous: true.
///
/// The [ExampleMonitor] stops the timer when it reaches 5 ticks, and the output is
/// therefore limited to 'Ho,Ho,Ho,Ho,Ho'.
///
/// Note that here we use timer.waitUntilDone() instead of timer.waitForFinish().
/// With a continuous output, waitForFinished() will never happen because there
/// is no finish, the timer has to be stopped. timer.waitUntilDone() will complete
/// when the timer is stopped or finishes naturally.
Future<void> runContinuousFixedInputTimer() async {
  print('runContinuousFixedInputTimer');
  final timer = FixedTimer<String,String>(
    tickInterval: const Duration(milliseconds: 100),
    inputValue: 'Ho',
    continuous: true,
  );
  final monitor = ExampleMonitor<String,String>(timer, stopAfter: 5);
  monitor.monitor(timer.output);
  await timer.start();
  await timer.waitUntilDone();

  print('total ticks: ${monitor.count}');
  print('----');
}

class ExampleMonitor<IN,OUT> {
  ExampleMonitor(this.timer, {this.stopAfter = -1});
  final int stopAfter;

  final VersatileTimer<IN,OUT> timer;
  int count = 0;

  void monitor(Stream<OUT> stream) {
    stream.listen((data) {
      count++;
      // ignore: noop_primitive_operations
      print(data.toString());
      if (stopAfter != -1 && count >= 5) {
        timer.stop();
      }
    });
  }
}

void main() async {
  await runLimitedFixedInputTimer();
  await runLimitedFixedInputTimerTransformed();
  await runContinuousFixedInputTimer();
}
