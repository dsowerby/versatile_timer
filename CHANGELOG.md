## 1.0.1

### Corrections for publishing

- Example.md separate
- Dart version in pubspec.yaml
- Description in pubspec.yaml
- README.md updated

## 1.0.0

- Initial version.
