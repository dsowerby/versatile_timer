import '../pattern/pattern_config.dart';
import '../pattern/pattern_filter.dart';
import '../pattern/weighted_timer.dart';

/// Different sub-classes take different forms of input and then uses a [transformer]
/// to modify the input as required.
///
/// [maxTicks] is the maximum number of ticks to run in total, whether or not
/// a repeat has occurred.
///
/// If [continuous] is true, [maxTicks] is ignored, and the parent timer will
/// run until it is stopped or paused.
///
/// The [processor] takes care of iterating the input [repeat] times, and ensures
/// that [maxTicks] is not exceeded
///
/// [IN] is the type of value passed to the filter
/// [OUT] is the type of value returned for each tick
abstract class TickFilter<IN, OUT> {
  TickFilter({
    required this.maxTicks,
    required List<PatternElement> pattern,
    required int repeat,
    required TickTransform<IN, OUT>? transformer,
    required bool continuous,
  }) : processor = GroupProcessor<IN, OUT>(
          group: Group(
            pattern: pattern,
            repeat: repeat,
            label: 'root',
          ),
          transformer: transformer,
          continuous: continuous,
        );
  final GroupProcessor<IN, OUT> processor;

  final int maxTicks;

  OUT? tick(int count) {
    final OUT? t = transformInput(count);
    checkFinished(count);
    return t;
  }

  TickTransform<IN, OUT>? get transformer => processor.transformer;

  bool _isFinished = false;

  bool get isFinished => _isFinished;

  bool get isNotFinished => !_isFinished;

  /// Configuration prevents any output being generated. This allows the parent
  /// timer to generate zero output rather than failing.
  bool get isEmpty =>
      maxTicks == 0 ||
      processor.group.pattern.isEmpty ||
      processor.group.repeat == 0;

  bool get isNotEmpty => !isEmpty;

  /// Where a filter uses configuration, this is the place to validate the
  /// configuration.  Returns an empty list if the configuration is valid
  List<String> validate();

  /// Gets the data from source and transforms it to the type required for output
  /// [count] is the total count of ticks so far.  Can be null if the transformation
  /// so chooses.  This effectively mute the timer output for this tick.
  OUT? transformInput(int count);

  /// check whether the filter has finished processing by reaching [maxTicks].
  /// To do that we increment the count and test for failure.
  ///
  ///  Sets [isFinished].
  ///
  /// This requires that sub-classes set [maxTicks] appropriately,
  void checkFinished(int count) {
    final int c = count + 1;
    _isFinished = c >= maxTicks;
  }
}

/// A common base class for the 3 provided types of transformation
///
/// [IN] is the input
/// [OUT] is the output
abstract class TickTransform<IN, OUT> {
  OUT? transform({required int count, required IN input});
}

/// Implementations provide a fixed input value of type [IN] which is then
/// transformed to an output of [OUT].
abstract class FixedTransform<IN, OUT> extends TickTransform<IN, OUT> {}

/// Implementations provide an output of type [OUT] based on the supplied [DATA].
/// Typically [count] is used to look up the value, but that does depend on
/// the implementation.
abstract class DataTransform<DATA, OUT> extends TickTransform<DATA, OUT> {}

/// Implementations use a configuration object to determine the output.
/// The configuration is usually used as a template to provide a pattern of output
/// 'ticks'. The transformer then determines how to represent the ticks, based on
/// configuration.  The [WeightedTimer] is an example of this
abstract class ConfigTransform<CONFIG, OUT>
    extends TickTransform<CONFIG, OUT> {}
