import '../filter/tick_filter.dart';
import '../timer/versatile_timer.dart';
import 'fixed_filter.dart';

/// A convenience class, a shortcut to constructing [VersatileTimer] with a
/// [FixedFilter].  It will run until [maxTicks] is reached, or if
/// [continuous] is set, it will run until [stop] or [pause] is called.
///
/// The [output] is a [Stream] of [inputValue] modified as required by
/// [transformer].  A transformer is not usually used unless it modifies
/// the [inputValue] in relation to something external - time perhaps
class FixedTimer<IN, OUT> extends VersatileTimer<IN, OUT> {
  FixedTimer({
    required super.tickInterval,
    super.continuous = false,
    required IN inputValue,
    FixedTransform<IN, OUT>? transformer,
    int maxTicks = 10,
  }) : super(
          tickFilter: FixedFilter<IN, OUT>(
            maxTicks: maxTicks,
            repeat: 1,
            inputValue: inputValue,
            transformer: transformer,
          ),
        );
}
