import '../filter/tick_filter.dart';
import '../pattern/pattern_config.dart';

/// The most basic implementation.  A fixed [inputValue] is passed to the
/// output via a [transformer].
class FixedFilter<IN, OUT> extends TickFilter<IN, OUT> {
  FixedFilter({
    required super.maxTicks,
    required FixedTransform<IN, OUT>? transformer,
    super.continuous = false,
    required this. inputValue,
    required super.repeat,
  }) : super(pattern: [
          DefaultPatternStep(data: inputValue, label: 'basic', repeat: maxTicks)
        ], transformer: transformer);
  final IN inputValue;

  @override
  List<String> validate() {
    /// Nothing to validate
    return [];
  }

  @override
  OUT? transformInput(int count) {
    final tx = transformer;
    final OUT? t = (tx == null)
        ? inputValue as OUT
        : tx.transform(count: count, input: inputValue);
    return t;
  }
}
