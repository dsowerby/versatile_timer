import '../filter/tick_filter.dart';
import '../timer/versatile_timer.dart';
import 'indexed_filter.dart';

/// A convenience class, a shortcut to constructing [VersatileTimer] with a
/// [IndexedFilter].  It will run until [maxTicks] is reached, or if
/// [continuous] is set, it will run until [stop] or [pause] is called.
///
/// If [maxTicks] is not set it will finish once all [data] has been processed
///
/// Note that [maxTicks] may be modified if it would cause data lookup to go out
/// of bounds.  See [IndexedFilter]
///
/// The [output] is a [Stream] of [outputValue]
class IndexedTimer<DATA, OUT> extends VersatileTimer<DATA, OUT> {
  IndexedTimer({
    required super.tickInterval,
    super.continuous = false,
    required List<DATA> data,
    int? maxTicks,
    DataTransform<DATA, OUT>? transformer,
  }) : super(
          tickFilter: IndexedFilter<DATA, OUT>(
            maxTicks: maxTicks,
            data: data,
            transformer: transformer,
            continuous: continuous,
          ),
        );
}
