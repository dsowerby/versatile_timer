import '../filter/tick_filter.dart';
import '../pattern/pattern_config.dart';

/// The count of ticks from the parent timer is used as an index to lookup
/// an element from [data]
///
/// [maxTicks] determines the number of ticks output for each repeat.
///
/// To avoid index out of bounds errors,  [maxTicks] cannot be set to greater
/// than the length of data or less than 0. If it is, it is reset to [data.length].
///
/// You can call stop, pause and resume after start has been called.
class IndexedFilter<DATA, OUT> extends TickFilter<DATA, OUT> {
  IndexedFilter({
    int? maxTicks,
    super.continuous = false,
    DataTransform<DATA, OUT>? super.transformer,
    required this.data,
    super.repeat = 1,
  }) : super(
          maxTicks:
              (maxTicks != null && maxTicks >= 0 && maxTicks <= data.length)
                  ? maxTicks
                  : data.length,
          pattern: [
            DefaultPatternStep(
              data: data,
              label: 'indexed',
              repeat: repeat,
            )
          ],
        );

  final List<DATA> data;

  @override
  List<String> validate() {
    /// Nothing to validate
    return [];
  }

  @override
  OUT? transformInput(int count) {
    final DATA d = data[count];
    final transform = transformer;
    if (transform != null) {
      return transform.transform(count: count, input: d);
    } else {
      return d as OUT;
    }
  }
}
