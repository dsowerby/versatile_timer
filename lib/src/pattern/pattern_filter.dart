import '../filter/tick_filter.dart';
import 'pattern_config.dart';

class PatternFilter<CONFIG extends PatternConfig, IN, OUT>
    extends TickFilter<IN, OUT> {
  PatternFilter({
    required CONFIG config,
    required super.transformer,
    required super.continuous,
  }) : super(
          pattern: config.pattern,
          maxTicks: config.maxTicks,
          repeat: config.repeat,
        );

  @override
  List<String> validate() {
    final List<String> messages = List.empty(growable: true);
    for (final element in processor.group.pattern) {
      element.validate(messages);
    }
    return messages;
  }

  @override
  OUT? transformInput(int count) {
    return processor.tick(count);
  }
}

abstract class ElementProcessor<OUT> {
  OUT? tick(int count);

  int _repeatIndex = 0;
  bool _isFinished = false;

  bool get isFinished => _isFinished;

  bool get isNotFinished => !_isFinished;
}
/// If [transformer] is null, the [input] is returned as-is.  This does require
/// [DATA] and [OUT] are the same.
class StepProcessor<DATA, OUT> extends ElementProcessor<OUT> {
  StepProcessor({
    required this.input,
    required this.transformer,
    required this.repeat,
  });

  final TickTransform<DATA, OUT>? transformer;
  final PatternStep<DATA> input;
  final int repeat;

  /// A step only ever handles a single tick, so it is the [input].repeat value
  /// that can cause multiple iterations
  @override
  OUT? tick(int count) {
    final tx = transformer;
    final OUT? t = (tx == null)
        ? input.data as OUT
        : tx.transform(count: count, input: input.data);

    _repeatIndex++;
    _isFinished = _repeatIndex >= repeat;
    return t;
  }
}

/// [continuous] should only ever be true for the root processor.
class GroupProcessor<IN, OUT> extends ElementProcessor<OUT> {
  GroupProcessor(
      {required this.group,
      required this.transformer,
      this.continuous = false}) {
    init();
  }

  final bool continuous;

  final TickTransform<IN, OUT>? transformer;
  int _patternIndex = 0;
  final Group group;
  late ElementProcessor<OUT> activeProcessor;

  void init() {
    _selectProcessor();
  }

  @override
  OUT? tick(int count) {
    final t = activeProcessor.tick(count);
    if (activeProcessor.isFinished) {
      _patternIndex++;
      if (_patternIndex >= group.pattern.length) {
        _repeatIndex++;
        _patternIndex = 0;
        _selectProcessor();
        if (!continuous && _repeatIndex >= group.repeat) {
          _isFinished = true;
        } else {
          _selectProcessor();
        }
      } else {
        _selectProcessor();
      }
    }
    return t;
  }

  void _selectProcessor() {
    final PatternElement element = group.pattern[_patternIndex];
    if (element is PatternStep) {
      activeProcessor = StepProcessor(
        input: element,
        transformer: transformer,
        repeat: element.repeat,
      );
    }
    if (element is Group) {
      activeProcessor =
          GroupProcessor(group: element, transformer: transformer);
    }
  }
}
