import '../filter/tick_filter.dart';
import '../timer/versatile_timer.dart';
import 'pattern_filter.dart';
import 'weighted_config.dart';

/// Provides a [T] output for each tick input using [transformer].
///
/// Three transformers are defined:
///
/// - [WeightValueTransformer]
/// - [LabelTickTransformer]
/// - [WeightTransformer]
///
class WeightedTimer<OUT> extends VersatileTimer<Weight,OUT> {
  /// Setting [continuous] to true will cause the output to continue until the
  /// timer is interrupted with [pause] or [stop].
  /// This overrides the root [Group.repeat] of [config], which normally determines
  /// how many times the output is repeated.
  ///
  /// [transformer] is used to transform a tick into the return value of type [T]
  WeightedTimer({
    required super.tickInterval,
    required WeightedConfig config,
    super.stateListeners,
    super.continuous,
     TickTransform<Weight,OUT>? transformer,
  }) : super(
            tickFilter: PatternFilter<WeightedConfig, Weight,OUT>(
          config: config,
          transformer: transformer,
          continuous: continuous,
        ));
}

/// Returns just the [Weight.weight]
class WeightValueTransformer implements ConfigTransform< Weight,int> {
  const WeightValueTransformer();

  @override
  int transform({required int count,required Weight input}) {
    return input.weight;
  }
}

/// Returns just the [Weight.label]
class WeightLabelTransformer implements ConfigTransform< Weight,String> {
  const WeightLabelTransformer();

  @override
  String transform({required int count,required Weight input}) {
    return input.label;
  }
}

/// Returns the whole [Weight]
class WeightTransformer implements ConfigTransform<Weight, Weight> {

  const WeightTransformer();

  @override
  Weight transform({required int count,required Weight input}) {
    return input;
  }
}
