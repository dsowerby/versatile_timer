import 'pattern_config.dart';

class WeightedConfig extends PatternConfig {
  const WeightedConfig({required super.pattern, super.repeat = 1});
}

class Weight extends PatternStep<Weight> {
  const Weight(this.weight, {super.repeat = 1, super.label = ''});

  final int weight;

  @override
  String toString() {
    return 'Weight: $label,weight: $weight, repeat: $repeat';
  }

  @override
  void validate(List<String> messages) {
    /// Nothing to validate
  }



  @override
  int get ticksPerRepeat => 1;

  /// This slightly odd way of returning the data makes it possible to use
  /// a
  @override
  Weight get data => this;
}
