import '../common/common.dart';
import '../filter/tick_filter.dart';
import 'pattern_filter.dart';

/// Common base for all elements of a pattern including [PatternConfig]
abstract class PatternBase {
  const PatternBase({required this.repeat});

  final int repeat;

  int get maxTicks => ticksPerRepeat * repeat;

  int get ticksPerRepeat;

  void validate(List<String> messages);
}

/// No functional difference to [PatternBase], just used to distinguish
/// elements which are contained within a [PatternConfig]
abstract class PatternElement extends PatternBase {
  const PatternElement({required super.repeat, required this.label});

  final String label;
}

/// Provides the tick input, which is then passed to a [TickTransform] before
/// being placed in the output stream of the timer
///
/// Cannot contain other elements, Unlike [Group]
abstract class PatternStep<IN> extends PatternElement {
  const PatternStep({required super.label, required super.repeat});
  @override
  int get ticksPerRepeat => 1;
  IN get data;
}

class DefaultPatternStep<IN> extends PatternStep<IN> {

  const DefaultPatternStep({
    required this.data,
    required super.label,
    required super.repeat,
  });
  @override
  final IN data;

  @override
  void validate(List<String> messages) {
    // Nothing to validate
  }
}

class TimerConfigException extends VersatileTimerException {
  const TimerConfigException(super.message);
}

/// Provides a collection of [PatternElement]s. which may include other [Group]s.
/// thus creating a tree structure.
///
/// A [Group] must not be empty.
class Group extends PatternElement {
  const Group({required this.pattern, super.repeat = 1, super.label = ''});

  final List<PatternElement> pattern;

  @override
  void validate(List<String> messages) {
    if (pattern.isEmpty) {
      messages.add('Group $label cannot be empty');
    }
  }

  bool get isEmpty => pattern.isEmpty;

  /// Returns the sum of maxTicks property of all elements in [pattern]
  @override
  int get ticksPerRepeat => pattern.fold(
      0, (previousValue, element) => previousValue + element.maxTicks);
}

/// A [PatternConfig] implementation provides a tree-like configuration.
/// It is defined by steps (which must implement [PatternStep]) and [Group]s.
/// Groups may contain other Groups or steps, thereby creating a tree structure.
///
/// A Group is not required, [pattern] can just be a list of [PatternStep].
///
/// This configuration supports [PatternFilter].
abstract class PatternConfig {
  const PatternConfig({required this.pattern, required this.repeat});

  final List<PatternElement> pattern;

  final int repeat;

  /// Returns the sum of maxTicks property of all elements in [pattern]
  int get ticksPerRepeat => pattern.fold(
      0, (previousValue, element) => previousValue + element.maxTicks);

  int get maxTicks => repeat * ticksPerRepeat;
}
