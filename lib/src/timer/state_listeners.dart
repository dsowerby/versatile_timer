
import '../../versatile_timer.dart';

abstract class TimerStateListener {
  /// Return true if the listener is still required.  If false, listener will
  /// be removed once notifications are complete
  bool onStateChange({
    required TimerState fromState,
    required TimerState toState,
  });
}

/// Will invoke [callback] when the state changes to any of the [targetStates]
class TargetStateListener implements TimerStateListener {
  TargetStateListener({
    required this.targetStates,
    required this.callback,
    this.invocations = -1,
  });

  final List<TimerState> targetStates;
  final void Function() callback;
  final int invocations;
  int _called = 0;

  /// Returns [continueListening], which if false, causes this to be removed
  /// from the parent timer's state listeners once notifications are complete
  @override
  bool onStateChange({
    required TimerState fromState,
    required TimerState toState,
  }) {
    if (targetStates.contains(toState)) {
      callback();
      _called++;
      return continueListening;
    }
    return true;
  }

  bool get continueListening {
    if (invocations < 0) {
      return true;
    }
    return _called < invocations;
  }
}
