import 'dart:async';

import '../common/common.dart';
import '../filter/tick_filter.dart';
import '../pattern/pattern_config.dart';
import 'state_listeners.dart';

/// [VersatileTimer]  class provides the [start], [stop], [pause]
/// and [resume] capability, based on a Dart periodic [Timer] set to a 'tick'
/// rate specified by [tickInterval].
///
/// Each tick is passed to a [tickFilter] which may modify the tick to represent
/// it however it wishes.  The type returned on the [output] Stream is specified
/// by [OUT] and the input value type by [IN].
///
/// The rate of tick output is determined by [tickInterval], which may also be
/// adjusted while the timer is running with a call to [changeSpeed].  When the
/// speed is changed the internal Dart Timer is cancelled and reconstructed,
/// which may cause some loss of overall timing accuracy.
///
/// A [VersatileTimer] is initiated with a call to [start], and then continues
/// until either the [tickFilter] indicates that it is finished, or it is
/// interrupted by [pause] or [stop].
///
/// Selecting different [tickFilter] implementations allow different ways of
/// configuring the output of the timer.
///
/// If [stateLogging] is true, the timer will log the current state of the timer.
///
/// When a timer [isDone, it is frozen, and throws a [VersatileTimerException]
/// if an attempt is made to call anything which would change its state.

class VersatileTimer<IN, OUT> {
  VersatileTimer({
    required Duration tickInterval,
    this.continuous = false,
    List<TimerStateListener> stateListeners = const [],
    required this.tickFilter,
    this.stateLogging = false,
  }) : _tickInterval = tickInterval {
    _validate();
    _stateListeners.addAll(stateListeners);
    _initializeStreamControllers();
  }

  final bool stateLogging;
  final bool continuous;
  Duration _tickInterval;
  TimerState _state = TimerState.waiting;
  DateTime _startedAt = zeroDate;
  DateTime _endedAt = zeroDate;
  late Timer _timer;
  final TickFilter<IN, OUT> tickFilter;
  int _tickCount = 0;

  final List<TimerStateListener> _stateListeners = List.empty(growable: true);
  late StreamController<OUT> _outputController =
      StreamController<OUT>.broadcast();

  Stream<OUT> get output => _outputController.stream;

  /// Returns copy of [_stateListeners] to avoid accidental adding or removing
  /// outside of the timer
  List<TimerStateListener> get stateListeners => List.from(_stateListeners);

  Duration get tickInterval => _tickInterval;

  void addStateListener(TimerStateListener listener) {
    _stateListeners.add(listener);
  }

  /// Starts the timer running with tick interval set by [_tickInterval]
  ///
  /// A [VersatileTimerException] is thrown if the current state is not
  /// [TimerState.waiting]
  ///
  /// A timer cannot be re-started once [isDone] is true
  ///
  /// If [tickFilter].isEmpty is true, the timer is not started, but instead
  /// switches immediately to a [TimerState.finished]. A [tickFilter] will return
  /// isEmpty==true when its configuration would prevent any output being generated.
  Future<TimerState> start() {
    if (state == TimerState.waiting) {
      final nextState =
          tickFilter.isEmpty ? TimerState.finished : TimerState.running;
      _changeState(nextState);
      if (nextState == TimerState.finished) {
        log.i(
            'TickFilter has nothing to generate, switching immediately to $nextState');
      }
      _startedAt = DateTime.now();
      _runTimer();
      return waitForState([nextState]);
    }
    throw const VersatileTimerException(
        'VersatileTimer can only be started from a waiting state');
  }

  /// Validates configuration of the timer and [tickFilter].  If you define your
  /// own configuration or [TickFilter] implementation, you must also define its
  /// validation
  void _validate() {
    final errorMessages = tickFilter.validate();
    if (errorMessages.isNotEmpty) {
      final errors = errorMessages.join('\n');
      final String msg = 'Validation failed with these errors:\n$errors';
      log.e(msg);
      throw TimerConfigException(msg);
    }
  }

  /// Pause until [resume] or [stop]. The internal[_timer] is cancelled, so no
  /// ticks are generated, allowing a [resume] to pick up from this point.
  ///
  /// A [VersatileTimerException] will be thrown if the current state is not one
  /// of:
  /// -[TimerState.running]
  Future<TimerState> pause() async {
    _checkState({TimerState.running}, 'pause');
    _timer.cancel();
    _changeState(TimerState.paused);
    return waitForState([TimerState.paused]);
  }

  void _runTimer() {
    _timer = Timer.periodic(_tickInterval, (timer) {
      _handleTick();
    });
  }

  /// Once stopped,as defined by [isDone], timer cannot be re-used.
  ///
  /// A [VersatileTimerException] will be thrown if the current state is not one
  /// of:
  /// - [TimerState.paused],
  /// - [TimerState.running]
  /// - [TimerState.waiting]}
  Future<TimerState> stop() {
    _checkState(
      {
        TimerState.paused,
        TimerState.running,
        TimerState.waiting,
      },
      'stop',
    );
    _timer.cancel();
    _changeState(TimerState.stopped);
    _endedAt = DateTime.now();
    return waitForState([TimerState.stopped]);
  }

  /// Resumes a [pause].
  ///
  /// A [VersatileTimerException] will be thrown if the current state is not one
  /// of:
  /// - [TimerState.paused]
  Future<TimerState> resume() {
    _checkState(
      {TimerState.paused},
      'resume',
    );
    _changeState(TimerState.running);
    _runTimer();
    return waitForState([TimerState.running]);
  }

  void _closeStreams() {
    _outputController.close();
  }

  void _initializeStreamControllers() {
    _outputController = StreamController<OUT>.broadcast();
  }

  void _handleTick() {
    final OUT? t = tickFilter.tick(_tickCount);
    if (t != null) {
      _outputController.add(t);
    }
    tickFilter.checkFinished(_tickCount);
    if (!continuous && tickFilter.isFinished) {
      _changeState(TimerState.finished);
      _endedAt = DateTime.now();
      _timer.cancel();
      _closeStreams();
      return;
    }
    _tickCount++;
  }

  /// Wait for a state contained in [targetStates].  This is effectively a logical
  /// OR.
  Future<TimerState> waitForState(List<TimerState> targetStates) async {
    // Completer is used to complete the Future when the target value is reached.
    final completer = Completer<TimerState>();

    // Check if the property is already at the target value.
    if (targetStates.contains(state)) {
      completer.complete(state);
    } else {
      // Create a listener.
      final listener = TargetStateListener(
          targetStates: targetStates,
          callback: () => completer.complete(state),
          invocations: 1);
      // Add the listener
      _stateListeners.add(listener);
    }

    return completer.future;
  }

  /// Waits for the timer to reach the [TimerState.finished] state.  **NOTE:** This
  /// will wait indefinitely if [continuous] is true, use [waitUntilStopped] instead
  Future<TimerState> waitUntilFinished() {
    return waitForState([TimerState.finished]);
  }

  /// Waits for the timer to reach the [TimerState.stopped] state. If the timer is
  /// expected to finish naturally use [waitUntilFinished]
  Future<TimerState> waitUntilStopped() {
    return waitForState([TimerState.stopped]);
  }

  /// Waits for the timer to reach either [TimerState.stopped] OR [TimerState.finished]
  Future<TimerState> waitUntilDone() {
    return waitForState([TimerState.stopped, TimerState.finished]);
  }

  void removeStateListener(TimerStateListener listener) {
    _stateListeners.remove(listener);
  }

  void _checkState(Set<TimerState> validStates, String requested) {
    if (validStates.contains(state)) {
      return;
    }
    final msg =
        "In order to $requested, must be in one these states: {${validStates.map((e) => e.toString().split('.')[1]).join(',')}} but is '${state.toString().split('.')[1]}'";
    throw VersatileTimerException(msg);
  }

  /// Change state and fire [_stateListeners].  If a listener returns false, it
  /// is removed from  [_stateListeners] once notifications complete.
  ///
  /// If [stateLogging] is true, the change is logged
  void _changeState(TimerState toState) {
    final fromState = state;
    _state = toState;
    if(stateLogging) {
      log.i('State changed from $fromState to $toState');
    }
    final List<TimerStateListener> toRemove = List.empty(growable: true);
    for (final TimerStateListener listener in _stateListeners) {
      final bool keep =
          listener.onStateChange(fromState: fromState, toState: toState);
      if (!keep) {
        toRemove.add(listener);
      }
    }
    toRemove.forEach((element) {
      _stateListeners.remove(element);
    });
  }

  /// Changes the time interval between ticks, and therefore the speed of the timer.
  ///
  /// A [VersatileTimerException] will be thrown if the current state is not one
  /// of:
  /// - [TimerState.paused]
  /// - [TimerState.running]
  void changeSpeed(Duration newInterval) {
    _checkState({
      TimerState.paused,
      TimerState.running,
    }, 'changeSpeed');
    _tickInterval = newInterval;
    if (_state == TimerState.running) {
      _timer.cancel();
      _runTimer();
    }
  }

  TimerState get state => _state;

  DateTime get startedAt => _startedAt;

  DateTime get endedAt => _endedAt;

  bool get isDone =>
      _state == TimerState.stopped || _state == TimerState.finished;

  bool get isWaiting => _state == TimerState.waiting;

  /// See also [isDone]
  bool get isFinished => _state == TimerState.finished;

  /// See also [isDone]
  bool get isStopped => _state == TimerState.stopped;

  bool get isRunning => state == TimerState.running;

  bool get isPaused => state == TimerState.paused;
}

/// - [TimerState.waiting] is the default state immediately after
/// construction of the timer, and remains in this state until [start] is
/// called, which changes the state to [TimerState.running].
///
/// - [TimerState.running] is the only state during which ticks are
/// passed to the [generator], and therefore to [listeners] and [interrupts].
/// Ticks are only passed through if the generator is not muted.
///
/// This state will continue until:
/// - A natural conclusion is reached by the [TickFilter] indicating it has
/// finished, or it is interrupted by the user code invoking [pause] or [stop]
///
/// - [TimerState.paused] is the state entered when [pause] is called.
/// The timer will remain in this state until a further call to either:
/// [resume] or [stop].
///
/// - [TimerState.stopped] occurs after a call to [PatternSetTimer.stop],
/// and will remain in this state. See also [TimerState.finished]
///
/// - [TimerState.finished] occurs when the [TickFilter] indicates it has
/// finished. It will remain in this state. See also [TimerState.stopped]
///
enum TimerState { waiting, paused, running, stopped, finished }
