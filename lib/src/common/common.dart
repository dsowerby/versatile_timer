import 'package:logger/logger.dart';

final DateTime zeroDate = DateTime.utc(0);

class VersatileTimerException implements Exception {
  const VersatileTimerException(this.message);

  final String message;
}

final log = Logger();
