# Notes

## Testing

To catch an error at some arbitrary point:

      runZonedGuarded(() async {
        await timer.start();
      }, (error, stack) {
        expect(error, isA<TimerConfigException>());
      });


### Coverage

With Flutter installed:

```bash
cd <project root>
flutter test --coverage
genhtml coverage/lcov.info -o coverage/html
```
file:///home/david/git/versatile_timer/coverage/html/index.html


StepProcessor to be given an input and transformer?
Perahps Step should contain a data field of type IN, so Weight is contained by Step rather than extending it
This would allow holding fixed value for Basic. How would it work for indexed.  Does Step.data hold the list of data?

