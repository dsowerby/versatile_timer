import 'dart:async';

import 'package:test/test.dart';
import 'package:versatile_timer/versatile_timer.dart';

void main() {
  late FixedTimer<int, int> timer;

  setUpAll(() => {
        // registerFallbackValue(defaultSetProperties)
      });
  setUp(() {
    timer = FixedTimer(
      tickInterval: const Duration(milliseconds: 100),
      inputValue: 1,
    );
  });

  tearDown(() {
    // if (!timer.isStopped && !timer.isWaiting) {
    //   timer.stop();
    // }
  });

  group('VersatileTimer', () {
    test('starts in waiting state', () {
      expect(timer.isDone, isFalse);
      expect(timer.state, TimerState.waiting);
    });
    test('starts, pauses, and resumes correctly', () async {
      timer.start();
      expect(timer.state, TimerState.running);

      await Future.delayed(const Duration(milliseconds: 50));

      timer.pause();
      expect(timer.state, TimerState.paused);

      timer.resume();
      expect(timer.state, TimerState.running);
    });

    test('stops correctly', () async {
      // given

      // when
      await timer.start();
      await Future.delayed(const Duration(milliseconds: 50));
      await timer.stop();

      // then
      expect(timer.state, TimerState.stopped);
    });

    test('further calls throw exception when stopped', () async {
      await timer.start();
      await timer.stop();
      expect(() => timer.start(), throwsA(isA<VersatileTimerException>()));
      expect(() => timer.pause(), throwsA(isA<VersatileTimerException>()));
      expect(() => timer.resume(), throwsA(isA<VersatileTimerException>()));
      expect(() => timer.changeSpeed(const Duration(milliseconds: 200)),
          throwsA(isA<VersatileTimerException>()));
    });

    test('changes speed', () async {
      // given

      // when
      await timer.start();
      await Future.delayed(const Duration(milliseconds: 150));
      // then
      timer.changeSpeed(const Duration(milliseconds: 50));
      expect(timer.tickInterval, const Duration(milliseconds: 50));
      expect(timer.isRunning, isTrue);
    });

    test('resume when not paused throw exception', () async {
      // given
      // when
      await timer.start();
      // then
      expect(() => timer.resume(), throwsA(isA<VersatileTimerException>()));

      // when
      await timer.stop();

      // then
      expect(() => timer.resume(), throwsA(isA<VersatileTimerException>()));

      // when
      final timer2 = FixedTimer<int, int>(
        tickInterval: const Duration(milliseconds: 20),
        inputValue: 1,
      );
      await timer2.start();

      // then
      expect(timer2.isRunning, isTrue);

      // when
      await timer2.waitUntilFinished();
      expect(() => timer.resume(), throwsA(isA<VersatileTimerException>()));
    });

    test('wait for state', () async {
      // given

      // when
      await timer.start();
      await timer.waitForState([TimerState.finished]);

      // then
      expect(timer.state, TimerState.finished);
    });

    test('start is allowed only from waiting', () async {
      // given

      // when
      await timer.start();
      // then
      expect(() => timer.start(), throwsA(isA<VersatileTimerException>()));
      // when
      await timer.pause();
      // then
      expect(() => timer.start(), throwsA(isA<VersatileTimerException>()));
      // when
      await timer.resume();
      await timer.waitForState([TimerState.finished]);
      // then
      expect(() => timer.start(), throwsA(isA<VersatileTimerException>()));
    });
    test('listeners are fired', () async {
      // given
      timer = FixedTimer(
        tickInterval: const Duration(milliseconds: 100),
        inputValue: 1,
      );
      final listener = StateListener();
      timer.addStateListener(listener);
      // when
      await timer.start();
      await timer.pause();
      await timer.resume();
      await timer.stop();
      // then

      expect(listener.states, [
        TimerState.running,
        TimerState.paused,
        TimerState.running,
        TimerState.stopped
      ]);
    });
    test('states correctly reported', () async {
      // given
      timer = FixedTimer(
        tickInterval: const Duration(milliseconds: 100),
        inputValue: 1,
      );
      // then before it is started

      expect(timer.isWaiting, true);
      expect(timer.isFinished, false);
      expect(timer.isStopped, false);
      expect(timer.isDone, false);
      expect(timer.isRunning, false);
      expect(timer.isPaused, false);

      // when
      await timer.start();
      // then
      expect(timer.isWaiting, false);
      expect(timer.isFinished, false);
      expect(timer.isStopped, false);
      expect(timer.isDone, false);
      expect(timer.isRunning, true);
      expect(timer.isPaused, false);

      // when
      await timer.pause();

      // then
      expect(timer.isWaiting, false);
      expect(timer.isFinished, false);
      expect(timer.isStopped, false);
      expect(timer.isDone, false);
      expect(timer.isRunning, false);
      expect(timer.isPaused, true);

      // when
      await timer.resume();

      // then
      expect(timer.isWaiting, false);
      expect(timer.isFinished, false);
      expect(timer.isStopped, false);
      expect(timer.isDone, false);
      expect(timer.isRunning, true);
      expect(timer.isPaused, false);

      // when
      await timer.stop();
      // then
      expect(timer.isWaiting, false);
      expect(timer.isFinished, false);
      expect(timer.isStopped, true);
      expect(timer.isDone, true);
      expect(timer.isRunning, false);
      expect(timer.isPaused, false);
    });
    test('Listener auto removes when it returns false', () async {
      // given
      // given
      timer = FixedTimer(
        tickInterval: const Duration(milliseconds: 10),
        inputValue: 1,
        continuous: true,
      );
      final listener1 = StateListenerRemoveOnState(TimerState.stopped);
      final listener2 = StateListenerRemoveOnState(TimerState.paused);
      timer.addStateListener(listener1);
      timer.addStateListener(listener2);
      // when
      await timer.start();

      // then
      expect(timer.stateListeners.length, 2);

      // when
      await timer.pause();

      // then
      expect(timer.stateListeners.length, 1);

      // when
      await timer.resume();
      await timer.stop();

      // then
      expect(timer.stateListeners.length, 0);
    });
    test('timestamps', () async {
      // given
      timer = FixedTimer(
        tickInterval: const Duration(milliseconds: 10),
        inputValue: 1,
        maxTicks: 50,
      );
      // when

      // then opening conditions
      expect(timer.startedAt, zeroDate);
      expect(timer.endedAt, zeroDate);

      // when
      await timer.start();

      // then
      expect(timer.startedAt.isAfter(zeroDate), true);
      expect(timer.endedAt, zeroDate);

      // when
      await timer.waitUntilFinished();

      // then
      expect(timer.startedAt.isAfter(zeroDate), true);
      expect(timer.endedAt.isAfter(zeroDate), true);
      expect(timer.endedAt.isAfter(timer.startedAt), true);
    });
  });
  test('waitUntilStopped', () async {
    // given
    timer = FixedTimer(
        tickInterval: const Duration(milliseconds: 10),
        inputValue: 1,
        continuous: true);

    // when
    delayedStop(timer);
    await timer.start();
    await timer.waitUntilStopped();

    // then
    expect(timer.isStopped, true);
  });
  test('transformer returns null', () async {
    // given
    final monitor  = TickMonitor('test');

    timer = FixedTimer<int, int>(
        tickInterval: const Duration(milliseconds: 10),
        inputValue: 1,
        maxTicks: 5,
        transformer: OddNumbersOnly());

    monitor.startMonitoring(timer.output);
    // when
    await timer.start();
    await timer.waitUntilDone();

    //
    expect(monitor.values, [1, 3]);
  });
}

class OddNumbersOnly implements FixedTransform<int, int> {
  @override
  int? transform({required int count, required int input}) {
    final int r = count % 2;
    return r == 0 ? null : count;
  }
}

Future<void> delayedStop(VersatileTimer<int, int> timer) async {
  Future.delayed(const Duration(seconds: 1), () {
    timer.stop();
  });
}

class TickMonitor {
  TickMonitor(this.label) : _tickCount = 0;
  final String label;
  final List<int> values = List.empty(growable: true);
  int _tickCount;
  late StreamSubscription<int> _subscription;

  void startMonitoring(Stream<int> stream) {
    _subscription = stream.listen((value) {
      _tickCount++;
      values.add(value);
    });
  }

  void stopMonitoring() {
    _subscription.cancel();
  }

  int get tickCount => _tickCount;
}

class StateListener implements TimerStateListener {
  final List<TimerState> states = List.empty(growable: true);

  /// Returns true to keep listening
  @override
  bool onStateChange(
      {required TimerState fromState, required TimerState toState}) {
    states.add(toState);
    return true;
  }
}

/// Returns false form [onStateChange] so that it is automatically removed after
/// [targetState] is reached
class StateListenerRemoveOnState implements TimerStateListener {
  StateListenerRemoveOnState(this.targetState);

  final TimerState targetState;
  final List<TimerState> states = List.empty(growable: true);

  /// Returns true to keep listening
  @override
  bool onStateChange(
      {required TimerState fromState, required TimerState toState}) {
    states.add(toState);
    return !(toState == targetState);
  }
}
