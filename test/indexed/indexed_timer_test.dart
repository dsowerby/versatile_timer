import 'dart:async';

import 'package:test/test.dart';
import 'package:versatile_timer/versatile_timer.dart';

void main() {
  group('IndexedTimer', () {
    late List<int> data;
    late StreamCounter<int> counter;

    setUp(() {
      counter = StreamCounter();
      data = List.generate(100, (index) => index);
    });

    test('maxTicks not set', () async {
      final timer = IndexedTimer<int, int>(
        tickInterval: const Duration(milliseconds: 10),
        data: data,
      );

      counter.countStream(timer.output);

      await timer.start();
      await timer.waitUntilDone();

      expect(counter.count, data.length);
      expect(counter.data, data);
      expect(timer.state, TimerState.finished);
    });

    test('maxTicks < data length', () async {
      final timer = IndexedTimer<int, int>(
        tickInterval: const Duration(milliseconds: 10),
        data: data,
        maxTicks: 50,
      );

      counter.countStream(timer.output);

      await timer.start();
      await timer.waitUntilDone();

      expect(counter.count, 50);
      expect(counter.data, data.sublist(0, 50));
      expect(timer.state, TimerState.finished);
    });

    test('maxTicks > data length throws exception', () async {
      // when & then
      try {} catch (e) {
        if (e is TimerConfigException) {
          expect(e.message, '??');
        } else {
          fail('Expected TimerConfigException');
        }
      }
    });
    test('maxTicks is ignored if less than zero', () async {
      final data = [1, 2, 3, 4, 5];
      final filter = IndexedFilter<int, int>(
        maxTicks: -1, // this is line 17
        data: data,
      );

      expect(filter.maxTicks, 5);
    });

    test('data is correctly exhausted', () async {
      final data = [1, 2, 3];
      final filter = IndexedFilter<int, int>(
        maxTicks: 5, // set maxTicks higher than data length to reach line 33
        data: data,
      );
      for (var i = 0; i < data.length; i++) {
        expect(filter.tick(i), data[i]);
      }
      expect(filter.isFinished, true);
    });
    test('Transformer changes the data type', () async {
      final counter = StreamCounter<String>();
      final timer = IndexedTimer<int, String>(
        tickInterval: const Duration(milliseconds: 10),
        data: List.generate(100, (i) => i),
        transformer: TestTransformer(),
      );

      counter.countStream(timer.output);

      await timer.start();
      await Future.delayed(
          const Duration(seconds: 2)); // Ensure timer has enough time to finish

      // We expect counter to count strings here, not ints, but the count should still be 100.
      expect(counter.count, 100);
    });
    test('maxTicks is set > data.length', () async {
      // given
      // when & then
      final timer = IndexedTimer<int, String>(
        tickInterval: const Duration(milliseconds: 10),
        data: [1, 2, 3, 4, 5],
        maxTicks: 6,
        transformer: TestTransformer(),
      );
      expect(timer.tickFilter.maxTicks, 5, reason: 'reverts to data.length');
    });
    test('maxTicks is set < 0', () async {
      // given
      // when & then
      final timer = IndexedTimer<int, String>(
        tickInterval: const Duration(milliseconds: 10),
        data: [1, 2, 3, 4, 5],
        maxTicks: -1,
        transformer: TestTransformer(),
      );
      expect(timer.tickFilter.maxTicks, 5, reason: 'reverts to data.length');
    });

    test('data is empty, timer finished without output', () async {
      // given
      final StreamCounter<String> counter=StreamCounter();
      final timer = IndexedTimer<int, String>(
        tickInterval: const Duration(milliseconds: 10),
        data: [],
        transformer: TestTransformer(),
      );
      // when
      counter.countStream(timer.output);
      await timer.start();
      await timer.waitUntilDone();
      // then
      expect(timer.state, TimerState.finished);
      expect(counter.count, 0);
    });
  });
}

class StreamCounter<T> {
  int count = 0;
  final List<T> data = [];

  Future<int> countStream(Stream<T> stream) async {
    await for (final T item in stream) {
      count++;
      data.add(item);
    }
    return count;
  }
}

class TestTransformer extends DataTransform<int, String> {
  @override
  String transform({required int count, required int input}) {
    return input.toString();
  }
}
