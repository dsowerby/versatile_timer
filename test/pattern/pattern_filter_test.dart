import 'dart:async';

import 'package:test/test.dart';
import 'package:versatile_timer/src/pattern/pattern_filter.dart';
import 'package:versatile_timer/versatile_timer.dart';

void main() {
  group('Weighted filter', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('calculate maxTicks', () {
      // given
      const WeightedConfig config = WeightedConfig(
        pattern: [
          Weight(1, repeat: 2, label: 'step 1'),
          Weight(2, repeat: 3, label: 'step 2'),
          Weight(3, label: 'step 3'),
        ],
        repeat: 2,
      );
      // when

      // then
      expect(config.pattern[0].ticksPerRepeat, 1);
      expect(config.pattern[0].maxTicks, 2);
      expect(config.pattern[1].ticksPerRepeat, 1);
      expect(config.pattern[1].maxTicks, 3);
      expect(config.pattern[2].ticksPerRepeat, 1);
      expect(config.pattern[2].maxTicks, 1);
      expect(config.ticksPerRepeat, 6);
      expect(config.maxTicks, 12);

    });
    test('isFinished', () {
      // given
      const WeightedConfig config = WeightedConfig(
        pattern: [
          Weight(1, repeat: 2, label: 'step 1'),
          Weight(2, repeat: 3, label: 'step 2'),
          Weight(3, label: 'step 3'),
        ],
        repeat: 2,
      );
      final PatternFilter<WeightedConfig, Weight, String> filter =
          PatternFilter(
        config: config,
        transformer: const WeightLabelTransformer(),
        continuous: false,
      );

      // when

      // then
      expect(filter.isFinished, false);
    });
    test('3 steps single layer', () {
      // given
      const WeightedConfig config = WeightedConfig(
        pattern: [
          Weight(1, repeat: 2, label: 'step 1'),
          Weight(2, repeat: 3, label: 'step 2'),
          Weight(3, label: 'step 3'),
        ],
        repeat: 2,
      );
      final PatternFilter<WeightedConfig, Weight, String> filter =
          PatternFilter(
        config: config,
        transformer: const WeightLabelTransformer(),
        continuous: false,
      );

      final List<String> output = [];

      // when
      int count = 0;
      while (filter.isNotFinished) {
        output.add(filter.tick(count)!);
        count++;
      }

      final outputString = output.join(',');

      // then

      expect(outputString,
          'step 1,step 1,step 2,step 2,step 2,step 3,step 1,step 1,step 2,step 2,step 2,step 3');
    });
    test('nested groups, multiple iterations', () {
      // given
      const expected1 =
          'step 1,step 1,step 2,step 2,step 2,step 3,step 1,step 1,step 2,step 2,step 2,step 3';
      const WeightedConfig config = WeightedConfig(
        pattern: [
          Group(
            label: 'Group A',
            repeat: 2,
            pattern: [
              Weight(1, repeat: 2, label: 'step 1'),
              Weight(2, repeat: 3, label: 'step 2'),
              Weight(3, label: 'step 3')
            ],
          ),
        ],
        repeat: 2,
      );
      final PatternFilter<WeightedConfig, Weight, String> runner =
          PatternFilter(
        config: config,
        transformer: const WeightLabelTransformer(),
        continuous: false,
      );

      final List<String> output = [];

      // when
      int count = 0;
      while (runner.isNotFinished) {
        output.add(runner.tick(count)!);
        count++;
      }

      final outputString = output.join(',');

      // then

      expect(outputString, '$expected1,$expected1');
    });
    test('continuous', () {
      // given
      const WeightedConfig config = WeightedConfig(
        pattern: [
          Weight(1, repeat: 2, label: 'step 1'),
          Weight(2, repeat: 3, label: 'step 2'),
          Weight(3, label: 'step 3'),
        ],
        repeat: 2,
      );
      final PatternFilter<WeightedConfig, Weight, int> filter = PatternFilter(
        config: config,
        transformer: const WeightValueTransformer(),
        continuous: true,
      );

      final List<String> output = [];

      // when
      for (int i = 1; i <= 20; i++) {
        output.add(filter.tick(i).toString());
      }

      // then

      expect(output.length, greaterThan(12));
    });
    test('empty Group throws exception', () async {
      // given

      expect(
          () => WeightedTimer<int>(
                tickInterval: const Duration(milliseconds: 10),
                config: const WeightedConfig(
                  pattern: [
                    Weight(1, repeat: 3),
                    Group(
                      pattern: [],
                      label: 'Group A',
                    ),
                  ],
                ),
                transformer: const WeightValueTransformer(),
              ),
          throwsA(isA<TimerConfigException>()));
      // when & then
    });
  });
}

class WeightedStreamCounter {
  int count = 0;

  Future<int> countStream(Stream<int> stream) async {
    await for (final _ in stream) {
      count++;
    }
    return count;
  }
}
