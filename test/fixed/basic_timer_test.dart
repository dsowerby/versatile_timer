import 'package:test/test.dart';
import 'package:versatile_timer/versatile_timer.dart';

import '../fixture/helper.dart';

void main() {
  group('BasicTimer', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('maxTicks', () async {
      // given
      final counter = StreamCounter();
      final timer = FixedTimer<int,int>(
        tickInterval: const Duration(milliseconds: 10),
        inputValue: 1,
      );
      counter.countStream(timer.output);
      // when
      await timer.start();
      await timer.waitUntilFinished();
      // then

      expect(counter.count, 10);
    });
    test('continuous', () async {
      // given
      final counter = StreamCounter();
      final timer = FixedTimer<int,int>(
        tickInterval: const Duration(milliseconds: 10),
        inputValue: 1,
        continuous: true,
      );
      counter.countStream(timer.output);
      // when
      await timer.start();
      await Future.delayed(const Duration(milliseconds: 200));
      await timer.stop();
      // then

      expect(counter.count, greaterThan(10));
    });
  });
}
