void tick(int count, void Function() target) {
  for (int i = 0; i < count; i++) {
    target();
  }
}

class StreamCounter {
  int count = 0;

  Future<int> countStream(Stream<int> stream) async {
    await for (final _ in stream) {
      count++;
    }
    return count;
  }
}
