import 'dart:async';

import 'package:test/test.dart';
import 'package:versatile_timer/versatile_timer.dart';

import '../fixture/helper.dart';

void main() {
  group('WeightedTimer', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('continuous ignores finish', () async {
      // given
      final counter = StreamCounter();
      final timer = WeightedTimer<int>(
        tickInterval: const Duration(milliseconds: 10),
        // ignore: avoid_redundant_argument_values
        continuous: true,
        config: const WeightedConfig(
          pattern: [Weight(1)],
        ),
        transformer: const WeightValueTransformer(),
      );
      counter.countStream(timer.output);
      // when
      await timer.start();
      await Future.delayed(const Duration(milliseconds: 100));

      // then
      expect(counter.count, greaterThan(1));
    });
    test('WeightTransformer returns whole weight', () {
      // given
      const weight = Weight(1, label: 'step 1');
      // when
      final actual = const WeightTransformer().transform(count: 1,input: weight);
      // then

      expect(actual, weight);
    });
    test('Weight to String', () {
      // given

      // when

      // then

      expect(const Weight(1, label: 'step 1').toString(),
          'Weight: step 1,weight: 1, repeat: 1');
    });
  });
}
